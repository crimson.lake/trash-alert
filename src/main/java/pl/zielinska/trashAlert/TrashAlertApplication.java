package pl.zielinska.trashAlert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrashAlertApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrashAlertApplication.class, args);
	}

}
